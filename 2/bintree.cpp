#include "bintree.h"
#include <stack>

void recurPrintTree(binTree * tree)
{
    if (tree == nullptr)
    {
        return;
    }
    recurPrintTree(tree->left);
    std::cout << tree->val << " ";
    recurPrintTree(tree->right);
}

void printTree(binTree * tree)
{
    std::stack<binTree*> treeStack;

    while (!treeStack.empty() || tree != nullptr)
    {
        if (tree != nullptr)
        {
            treeStack.push(tree->left);
            tree = tree->left;
        }
        else
        {
            binTree *node = treeStack.top();
            treeStack.pop();
            std::cout << node->val << " ";
            tree = tree->right;
        }
    }
}
