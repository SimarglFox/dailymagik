#pragma once
#include "../stdafx.h"
#include <iostream>
struct binTree {
    int val;
    binTree *left;
    binTree *right;
};

void recurPrintTree(binTree *tree);
void printTree(binTree *tree);