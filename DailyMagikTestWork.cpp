// DailyMagikTestWork.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <fstream>
#include <string>
#include "4\sudoku.h"
#include <iostream>

int main()
{
    std::string line;
    Puzzle sudoku;
    std::ifstream in("E:\\projects\\DailyMagikTestWork\\Debug\\sudoku.in");
    if (in.is_open())
    {
        while (getline(in, line))
        {
            std::vector<int> row;
            for (int i = 0; i < line.size(); i++)
            {
                int value = line.at(i) - '0';
                row.push_back(value);
            }
            sudoku.push_back(row);
        }
    }
    else
    {
        std::cout << "Error open in file" << std::endl;
        system("pause");
        return 0;
    }
    in.close();

    Puzzle solution = solve(sudoku);

    std::ofstream out;          // поток для записи
    out.open("sudoku.out"); // окрываем файл для записи
    if (out.is_open())
    {
        if (!solution.empty())
        {
            for (auto row : solution)
            {
                for (auto val : row)
                {
                    out << val;
                }
                out << std::endl;
            }
        }
        else
        {
            out << "Can't find solution";
            system("pause");
        }
    }
    else
    {
        std::cout << "Error open out file" << std::endl;
        system("pause");
    }
    return 0;
}