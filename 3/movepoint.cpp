#include "movepoint.h"
#include <cmath>

cPoint UpdatePoint(const float pathLenght, const cPoint * points, const int numPoinst)
{
    float pL = 0;
    for (int i = 0; i < numPoinst - 1; i++) {
        float pL1 = sqrt(pow((points[i].x - points[i + 1].x), 2) + pow((points[i].y - points[i + 1].y), 2));
        pL += pL1;
        if (pL >= pathLenght) {
            cPoint itogPoint;
            itogPoint.x = abs(points[i + 1].x - points[i].x)*(pL1 - pL + pathLenght) / pL1;
            itogPoint.y = abs(points[i + 1].y - points[i].y)*(pL1 - pL + pathLenght) / pL1;
            return itogPoint;
        }
    }
}
