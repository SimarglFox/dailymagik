#include "sudoku.h"

Puzzle solve(const Puzzle& puzzle) {
    Puzzle solution = puzzle;
    if (solveHelper(&solution)) {
        return solution;
    }
    return Puzzle();
}

bool solveHelper(Puzzle* solution) {
    int minRow = -1;
    int minColumn = -1;
    Values minValues;
    while (true) {
        minRow = -1;
        for (int rowIndex = 0; rowIndex < 9; ++rowIndex) {
            for (int columnIndex = 0; columnIndex < 9; ++columnIndex) {
                if ((*solution)[rowIndex][columnIndex] != 0) {
                    continue;
                }
                Values possibleValues = findPossibleValues(rowIndex, columnIndex, *solution);
                int possibleVaueCount = possibleValues.size();
                if (possibleVaueCount == 0) {
                    return false;
                }
                if (possibleVaueCount == 1) {
                    (*solution)[rowIndex][columnIndex] = *possibleValues.begin();
                }
                if (minRow < 0 || possibleVaueCount < minValues.size()) {
                    minRow = rowIndex;
                    minColumn = columnIndex;
                    minValues = possibleValues;
                }
            }
        }
        if (minRow == -1) {
            return true;
        }
        else if (1 < minValues.size()) {
            break;
        }
    }
    for (auto v : minValues) {
        Puzzle solutionCopy = *solution;
        solutionCopy[minRow][minColumn] = v;
        if (solveHelper(&solutionCopy)) {
            *solution = solutionCopy;
            return true;
        }
    }
    return false;
}

Values findPossibleValues(int rowIndex, int columnIndex, const Puzzle& puzzle) {
    Values values;
    for (int i = 1; i < 10; ++i) {
        values.insert(i);
    }
    Values badRowValues = getRowValues(rowIndex, puzzle);
    for (auto val : badRowValues)
    {
        values.erase(val);
    }
    Values badColumnValues = getColumnValues(columnIndex, puzzle);
    for (auto val : badColumnValues)
    {
        values.erase(val);
    }
    Values badBlockValues = getBlockValues(rowIndex, columnIndex, puzzle);
    for (auto val : badBlockValues)
    {
        values.erase(val);
    }
    return values;
}

Values getRowValues(int rowIndex, const Puzzle& puzzle) {
    Values itogValues;

    for (auto val : puzzle[rowIndex])
    {
        if (val != 0 && (itogValues.find(val) == itogValues.end()))
        {
            itogValues.insert(val);
        }
    }
    return itogValues;
}

Values getColumnValues(int columnIndex, const Puzzle& puzzle) {
    Values values;
    for (int r = 0; r < 9; ++r) {
        values.insert(puzzle[r][columnIndex]);
    }
    return values;
}

Values getBlockValues(int rowIndex, int columnIndex, const Puzzle& puzzle) {
    Values values;
    int blockRowStart = 3 * (rowIndex / 3);
    int blockColumnStart = 3 * (columnIndex / 3);
    for (int r = 0; r < 3; ++r) {
        for (int c = 0; c < 3; ++c) {
            values.insert(puzzle[blockRowStart + r][blockColumnStart + c]);
        }
    }
    return values;
}