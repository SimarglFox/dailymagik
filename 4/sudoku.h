#pragma once
#include "../stdafx.h"
#include <vector>
#include <set>

typedef std::vector<std::vector<int>> Puzzle;
typedef std::set<int> Values;

Puzzle solve(const Puzzle& puzzle);
bool solveHelper(Puzzle* solution);
Values findPossibleValues(int rowIndex, int columnIndex, const Puzzle& puzzle);
Values getRowValues(int rowIndex, const Puzzle& puzzle);
Values getColumnValues(int columnIndex, const Puzzle& puzzle);
Values getBlockValues(int rowIndex, int columnIndex, const Puzzle& puzzle);