#include "graph.h"

graphCheck::graphCheck()
{

}
graphCheck::~graphCheck()
{

}

bool graphCheck::isTree(graph *inputGraph)
{
    root = inputGraph;
    return treeFlag;
}

void graphCheck::graphStep(graph *inputNode)
{
    checkNode(inputNode);
    inputNode->checked = true;
    for (auto node : inputNode->childs)
    {
        if (!treeFlag)
        {
            return;
        }
        if (!node->checked)
        {
            graphStep(node);
        }
    }
}

void graphCheck::checkNode(graph *node)
{
    if (node->parents.size() == 0)
    {
        roots++;
        return;
    }
    if (node == root)
    {
        treeFlag = false;
        return;
    }
    if (roots > 1)
    {
        treeFlag = false;
        return;
    }
    if (node->parents.size() > 1)
    {
        treeFlag = false;
        return;
    }
}

