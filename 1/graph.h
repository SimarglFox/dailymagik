#pragma once
#include "../stdafx.h"
#include <vector>


struct graph{
    std::vector<graph*> parents;
    int someValue;
    bool checked;
    std::vector<graph*> childs;
};

class graphCheck
{
public:
    graphCheck();
    ~graphCheck();
    bool isTree(graph *inputGraph);
private:
    void graphStep(graph *inputNode);
    void checkNode(graph *node);

    graph *root;
    bool treeFlag = true;
    int roots = 0;
};